
#!/usr/bin/env python3A
#!/usr/bin/env python3


# -------
# imports
# -------
from Netflix import netflix_eval
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------

class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    def test_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        self.assertEqual(
            w.getvalue(), "10040:\n3.51\n3.41\n3.85\n0.86\n")

    def test_2_cornercase(self):
        r = StringIO("10039:\n102104\n")
        w = StringIO()
        netflix_eval(r,w)
        self.assertEqual(
            w.getvalue(), "10039:\n2.55\n1.55\n")

    def test_eval_3(self):
        r = StringIO("10:\n1952305\n1531863\n")
        w = StringIO()
        netflix_eval(r,w)
        self.assertEqual(
            w.getvalue(), "10:\n3.34\n3.16\n0.26\n")

    def test_eval_4(self):
        r = StringIO("10001:\n262828\n2609496\n1474804\n831991\n267142\n2305771\n220050\n1959883\n27822\n2570808\n90355\n2417258\n264764\n143866\n766895\n714089\n2350428\n")
        w = StringIO()
        netflix_eval(r,w)
        self.assertEqual(
            w.getvalue(), "10001:\n3.21\n4.07\n3.65\n2.95\n4.09\n3.45\n3.48\n3.89\n3.82\n3.4\n3.96\n3.96\n3.1\n3.99\n3.6\n3.63\n4.13\n0.93\n")
    
    def test_eval_5(self):
         r = StringIO("7851:\n2089595\n837161\n1962698\n2122080\n")
         w = StringIO()
         netflix_eval(r,w)
         self.assertEqual(
             w.getvalue(), "7851:\n3.23\n3.84\n3.5\n3.86\n1.04\n")

    def test_eval_6(self):
        r = StringIO("3646:\n1793946\n2534406\n2513903\n364810\n1361215\n2516882\n1938613\n703959\n623827\n1607320\n53134\n2257890\n267795\n57620\n1521772\n1289515\n187164\n368368\n389492\n171420\n1667961\n1149237\n275551\n")
        w = StringIO()
        netflix_eval(r,w)
        self.assertEqual(
                w.getvalue(), "3646:\n3.53\n2.42\n3.59\n3.9\n3.69\n3.62\n3.76\n3.41\n3.63\n3.21\n3.13\n3.35\n3.15\n3.03\n3.78\n3.65\n3.25\n3.55\n4.39\n4.01\n3.65\n4.37\n4.06\n1.14\n")



# ----
# main
# ----			
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
